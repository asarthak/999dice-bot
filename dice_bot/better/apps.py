from django.apps import AppConfig


class BetterConfig(AppConfig):
    name = 'dice_bot.better'
