from django.db import models
from django.contrib.auth.models import User
#Create your models here.

class Bets(models.Model):
    bet_data = models.TextField(null=False, default="{'error':'error'}");

class myUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bets = models.ForeignKey(Bets, on_delete=models.CASCADE)
