import json

from django.shortcuts import render, redirect
import requests
from django.http import HttpResponse
from django.contrib.auth.views import LoginView
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login

# Create your views here.

def index(request):
    if request.user.is_authenticated:
        response = requests.post('https://www.999dice.com/api/web.aspx',
                             data={'a': 'Login', 'key': '0f7a13d1c0e34e6a9a2bd24a8bb41ed2',
                                   'username': request.user.username, 'password': request.user.password})
        check_response = json.loads(response.content.decode('utf-8'))
        try:
            if check_response["LoginInvalid"] == 1:
                return HttpResponse("We could not find any 999dice account associated with that credentials. Please <a href='login/'>try again.</a>")
        except Exception as e:
            pass
        return render(request, 'index.html', context={'response': response.content.decode('utf-8')})
    else:
        return render(request,'index.html')




def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        request.user = user
        if user is not None:
            auth_login(request,user)
            return redirect('index')
        else:
            return HttpResponse("Incorrect Email and Password")
    return render(request, 'loginform.html')


def signup(request):
    if request.method == "POST":
        try:
            response = requests.post('https://www.999dice.com/api/web.aspx',
                                     data={'a': 'Login', 'key': '0f7a13d1c0e34e6a9a2bd24a8bb41ed2',
                                           'username': request.POST['username'], 'password': request.POST['password']})
            check_response = json.loads(response.content.decode('utf-8'))
            try:
                if check_response["LoginInvalid"] == 1:
                    return HttpResponse(
                        "We could not find any 999dice account associated with that credentials. Please <a href=''>try again.</a>")
            except Exception as e:
                pass
            myuser = User()
            myuser.username = request.POST['username']
            myuser.set_password(request.POST['password'])
            myuser.save()
            return render(request, 'index.html', context={'sucesss':'User sucessfully created! Login Now'})
        except Exception as e:
            print(e)

    return render(request, 'signupform.html')

def logout(request):
    pass
