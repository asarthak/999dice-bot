from rest_framework import serializers
from dice_bot.better.models import Bets

class BetsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bets
        fields = ('id', 'bet_data')
