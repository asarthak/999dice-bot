from django.urls import  path
from .views import CreateView, bet_create

urlpatterns = [
    path('create/', bet_create, name='create_bet_api'),
]
