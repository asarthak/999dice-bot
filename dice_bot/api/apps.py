from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'dice_bot.api'
