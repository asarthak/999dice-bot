from django.shortcuts import render
import requests

from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


from .serializers import BetsSerializer, Bets

# Create your views here.

class CreateView(generics.CreateAPIView):
    queryset = Bets.objects.all()
    serializer_class = BetsSerializer

    def perform_create(self, serializer):
        serializer.save()




@api_view(['GET', 'POST'])
def bet_create(request):

    if request.method == 'GET':
        if request.GET['choice'] == "high":
            bet_response = requests.post('https://www.999dice.com/api/web.aspx', data={
                        'a': "PlaceBet",
                        's': request.GET['session'],
                        "PayIn": request.GET['amount'],
                        "Low": 500500,
                        "High": 999999,
                        "ClientSeed": 123123,
                        "Currency": "btc",
                        "ProtocolVersion": 2
                    })
        elif request.GET['choice'] == "low":
            bet_response = requests.post('https://www.999dice.com/api/web.aspx', data={
                'a': "PlaceBet",
                's': request.GET['session'],
                "PayIn": request.GET['amount'],
                "Low": 0,
                "High": 499499,
                "ClientSeed": 123123,
                "Currency": "btc",
                "ProtocolVersion": 2
            })
            print(bet_response.content)
        # bets = Bets.objects.all()
        # serializer = BetsSerializer(bets,many=True)
        return Response(bet_response.content)

    elif request.method == 'POST':
        serializer = BetsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)