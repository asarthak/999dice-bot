from django.urls import include, path


urlpatterns = [
    path('', include('dice_bot.better.urls')),
    path('api/', include('dice_bot.api.urls')),
]
