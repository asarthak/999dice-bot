# 999Dice Bet Visualizer



999dice bet visualizer is a bet visualizer for bets placed on 999dice.com(through this application.)



#Features!

  - Real time profit chart.
  - Feature to place a bet on 999dice from within the app.
  - User Statistics table to show historical records about the user.


Steps to use this application:
  - Go to 999dice.com -> Accounts Tab - > Create a username or password
  - Create a username and password.
  - Claim free bitcoins from the faucet or deposit bitcoins in the address shown in the user statistics table.
    - To claim free bitcoins, goto 999dice.com -> Free bitcoins -> Claim Free Bitcoins buttom
  - Go to our applications signup page.
  - Sign up in our application with the same 999dice credentials.(compulsary)
  - Login to our application with the same credentials
  - Start betting



### Installation

```
git clone https://asarthak@bitbucket.org/asarthak/999dice-bot.git
```
Install virtualenv.
```
sudo apt-get install python-virtualenv
```

Then, start a new virtualenv.
```
virtualenv -p python3 dicebot_env
```

Time to activate the environment we created:
```
source dicebot_env/bin/activate
```

Then install libmysqlclient-dev.
```
apt install -y libmysqlclient-dev
```
Now install all requirements:
```
pip install -r requirements.txt
```

We now have to make a new file named .env and copy everything in it from env.example
```
cp env.example .env
```

Now, we fill our env vars inside the .env file we just made. For example
```
# Project Settings

DEBUG = True
SECRET_KEY = 'SOMESECRETHERE'

# Database Configurations

ENGINE = 'django.db.backends.mysql'
NAME = 'databasename'
USER = 'root'
PASSWORD = 'databasepassword'
HOST = '127.0.0.1'
PORT = 3306
```

Time to migrate!
```
python manage.py makemigrations
python manage.py migrate
```

LETS SEE THE RESULT!
```
python manage.py runserver
```

Visit localhost:8000 in your browser to view the result.
